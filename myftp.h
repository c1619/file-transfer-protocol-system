#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/sem.h>
#include <dirent.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>

// Client Functions
void ls();
void rls(int socketfd, char *servHostName);
void rcd(int socketfd, char *pathname);
void get(int socketfd, char *servHostName, char *pathname);
void put(int socketfd, char *servHostName, char *pathaname);
void show(int socketfd, char *servHostName, char *pathname);
void exitCommand(int socketfd);
int returnSocketFd(char *servHostName, char *portNumber);
int handleAcknowledge(int socketfd, char *ackString);
void commandError();

// Server Functions
int returnSocketFdServe(char *portNumber);
int setConnection(int backlog, int portNumber);
int setDataConnection(int socketfd);
void serveLs(int socketfd, int dataConnFd);
void serveGet(int socketfd, int dataConnFd, char *pathname);
void servePut(int socketfd, int dataConnFd, char *pathname);
void sendAcknowledgement(int socketfd, int isErr, char *errorOrPort);
void parseInput(int socketfd, char *pathname);

