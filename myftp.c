#include "myftp.h"

#define PORT_NUMBER_STRING "11235"

int main(int argc, char **argv) {
    char buffer[256];   // buffer to read inputted commands from the user to the client
    char *tokens[3];    // array of strings to store the tokens
    char delimiters[]=" \t\r\n\v\f";   // These are all the whitespace characters detected by isspace()
	int socketfd;	// this will be the socket that the client interacts with the server through

    // Making sure the user entered 
    if(argc < 2) {
        fprintf(stderr, "Usage: ./myftp <hostname>\n");
        return 0;
    }

    // Setting up the control connection
    socketfd = returnSocketFd(argv[1], PORT_NUMBER_STRING);
    if(socketfd < 0) {
        exit(0);
    }

    /** while loop to grab the input and respond accordingly **/
    int i;
    printf("MFTP> ");
    while(fgets(buffer, 255, stdin) != NULL) {
        i = 0;
        
        // Grabbing the tokens, making sure to not grab more than 
        tokens[i] = strtok(buffer, delimiters);
        while(tokens[i] != NULL && i != 2) {
            ++i;
            tokens[i] = strtok(NULL, delimiters);
        }

        // This would mean the user didn't input any tokens, in which case the user is simply prompted again
        if(tokens[0] == NULL) {
            printf("MFTP> ");
            continue;
        }

        /*************ls command****************/
        if(strcmp(tokens[0], "ls") == 0) { 
            ls();

        /************cd command****************/
        } else if(strcmp(tokens[0], "cd") == 0) {   // cd command
            // if tokens[1] == NULL that would mean the user did not enter a second token for the pathname
            if(tokens[1] == NULL) {
                commandError();
                continue;
            }
            
            // operate on cd normally now
            if(chdir(tokens[1]) != 0) {
                // Dealing with errors here
                fprintf(stderr, "Change Directory: %s\n", strerror(errno)); 
                printf("MFTP> ");
                continue;
            }
        
        /*************rls command******************/
        } else if (strcmp(tokens[0], "rls") == 0) { // rls command
            rls(socketfd, argv[1]);

        /**********rcd command **************/
        } else if(strcmp(tokens[0], "rcd") == 0) {  // rcd command
            // if tokens[1] == NULL that would mean the user did not enter a second token for the pathname
            if(tokens[1] == NULL) {
                commandError();
                continue;
            }

            rcd(socketfd, tokens[1]);
 
        /*********get command ************/
        } else if (strcmp(tokens[0], "get") == 0) {
            // if tokens[1] == NULL that would mean the user did not enter a second token for the pathname
            if(tokens[1] == NULL) {
                commandError();
                continue;
            }
            get(socketfd, argv[1], tokens[1]);

        /*********show command ************/
        } else if(strcmp(tokens[0], "show") == 0) { 
            // if tokens[1] == NULL that would mean the user did not enter a second token for the pathname
            if(tokens[1] == NULL) { 
                commandError();
                continue;
            }
            
            show(socketfd, argv[1], tokens[1]);
        /*********put command ************/
        } else if(strcmp(tokens[0], "put") == 0) {  
            // if tokens[1] == NULL that would mean the user did not enter a second token for the pathname
            if(tokens[1] == NULL) {
                commandError();
                continue;
            }

            put(socketfd, argv[1], tokens[1]);
        /**********exit command***********/
        } else if (strcmp(tokens[0], "exit") == 0) {
            exitCommand(socketfd);
        } else {
            printf("Command '%s' is unknown - ignored\n", tokens[0]);
        }
        printf("MFTP> ");
    }
   exitCommand(socketfd);
    return 0;
} 

void ls() {
    if(fork()) {
        wait(NULL);
    } else {
        int fd[2];          // pipe file descriptors (fd[0] is read and fd[1] is write)
        int rdr, wtr;       // fd[0] and fd[1]
         /*PIPE FUNCTIONALITY BEGINS HERE*/
        pipe(fd);   // filling in fd with file descriptor values. 
        rdr = fd[0]; wtr = fd[1];   // Setting up rdr and 

        if(fork()) {
            // this is the parent (which is reading from the child through the rdr file descriptor)
            wait(NULL);
            close(wtr);
            close(0); dup(rdr); close(rdr); // close stdin, then dup rdr which sets info of rdr to where stdin used to be, then close rdr. 
            execlp("more", "more", "-20", (char *)NULL);
            fprintf(stderr, "%s\n", strerror(errno));
            exit(1);
        } else {
            // this is the child (which is writing to the parent through the wtr file descriptor)
            close(rdr);
            close(1); dup(wtr); close(wtr); // close stdout, then dup wtr which sets information of wtr to where stdout used to be, then close wtr. 
            execlp ("ls", "ls", "-l", (char *)NULL);
            fprintf(stderr, "%s\n", strerror(errno));
            exit(1);
        }
    }
}

// Function to respond to user entering rls command. will create a data connection between server and client 
void rls (int socketfd, char *servHostName) {
    char portString[256]; // Stores the port number or error message given in the acknowledgement from the server
    int dataConnFd;
    
    // Writing to server to set up data connection
    write(socketfd, "D\n", 2);

    if(handleAcknowledge(socketfd, portString) < 0) {
        return;  
    }

    // printing out the more command from the ls of the server. 
    if(fork()) {
        int ret;
        wait(&ret);
        if(ret != 0) {
            exit(1);
        }
    } else {
        // note that the child will only exit with the exit code 1 if there was an error with regards to connecting to the server or creating the data connection. For all other errors or cases
        // it will simply exit with an exit code of 0. 
        // making a data connection
        dataConnFd = returnSocketFd(servHostName, portString);

        // if making the data connection fails then exit out of the child process 
        if(dataConnFd < 0) {
            exit(1);
        }

        // tell Server to run ls -l
        write(socketfd, "L\n", 2);
        
        // Reading the acknowledgement being sent from the server
        char ackString[256];
        if(handleAcknowledge(socketfd, ackString) < 0) {
            exit(0);
        }

        close(0); dup(dataConnFd); close(dataConnFd);
        execlp("more", "more", "-20", (char *)NULL);
        fprintf(stderr, "%s\n", strerror(errno));
        exit(0);
    } 
}

void rcd(int socketfd, char *pathname) {
    char commandString[256] =  "C"; // Stores the command that will be sent to the server which will be of the format "C<pathname>\n"
    char portString[256];
    
    // adding pathname and newline character to the command
    strcat(commandString, pathname);
    strcat(commandString, "\n");

    // Writing to server to set up data connection
    write(socketfd, commandString, strlen(commandString));
    handleAcknowledge(socketfd, portString);
}

void get(int socketfd, char *servHostName, char *pathname) {
    char commandString[256] =  "G"; // Stores the command that will be sent to the server which will be of the format "C<pathname>\n"
    char portString[256]; // Stores the port number or error message given in the acknowledgement from the server
    int dataConnFd;

    // adding pathname and newline character to the command
    strcat(commandString, pathname);
    strcat(commandString, "\n");

    // Writing to server to set up data connection
    write(socketfd, "D\n", 2);

    if(handleAcknowledge(socketfd, portString) < 0) {
        return;  
    }
    
    // printing out the more command from the ls of the server. 
    if(fork()) {
        int ret;
        wait(&ret);
        if(ret != 0) {
            exit(1);
        }
    } else {
        // note that the child will only exit with the exit code 1 if there was an error with regards to connecting to the server or creating the data connection. For all other errors or cases
        // it will simply exit with an exit code of 0. 
        // making a data connection
        dataConnFd = returnSocketFd(servHostName, portString);

        // if making the data connection fails then exit out of the child process 
        if(dataConnFd < 0) {
            exit(1);
        }

        // tell Server to run get command
        write(socketfd, commandString, strlen(commandString));
        
        // Reading the acknowledgement being sent from the server
        char ackString[256];
        if(handleAcknowledge(socketfd, ackString) < 0) {
            exit(0);
        }
        
        // opening a file of the same name of the file in the server
        // using O_CREAT and O_EXCL in order to make sure the file being created doesn't exist
        int fd = open(pathname, O_RDWR | O_CREAT | O_EXCL, 0777);
        if(fd < 0) {
            fprintf(stderr, "%s\n", strerror(errno));
            exit(0);
        }
        // loop to write everything from server to a new file created
        int numberread;
        char readBuf[256];
        numberread = read(dataConnFd, readBuf, 255);
        while(numberread != 0) {
            write(fd, readBuf, numberread);
            numberread = read(dataConnFd, readBuf, 255);
        }
        exit(0);
    }

}

void put(int socketfd, char *servHostName, char *pathname) {
    char commandString[256] =  "P"; // Stores the command that will be sent to the server which will be of the format "P<pathname>\n"
    char portString[256]; // Stores the port number or error message given in the acknowledgement from the server
    int dataConnFd;

    // Checking if the given file is a directory
    struct stat area; struct stat* s = &area;

    // first checks if the path for s is accessible
    if(access(pathname, R_OK | F_OK) < 0) {
        fprintf(stderr, "%s\n", strerror(errno));
        return;
    }

    // Checking to make sure that the pathname given isn't to a directory (if so, ignore the file and continue)
    if (lstat(pathname, s) == 0) {
        if (S_ISDIR(s->st_mode)) {
            fprintf(stderr, "%s\n", strerror(EISDIR));
            return;
        }
    } else {
        fprintf(stderr, "%s\n", strerror(errno));
        return;
    }


    // adding pathname and newline character to the command
    strcat(commandString, pathname);
    strcat(commandString, "\n");

    // Writing to server to set up data connection
    write(socketfd, "D\n", 2);

    if(handleAcknowledge(socketfd, portString) < 0) {
        return;  
    }
    
    // printing out the more command from the ls of the server. 
    if(fork()) {
        int ret;
        wait(&ret);
        if(ret != 0) {
            exit(1);
        }
    } else {
        // note that the child will only exit with the exit code 1 if there was an error with regards to connecting to the server or creating the data connection. For all other errors or cases
        // it will simply exit with an exit code of 0. 
        // making a data connection
        dataConnFd = returnSocketFd(servHostName, portString);

        // if making the data connection fails then exit out of the child process 
        if(dataConnFd < 0) {
            exit(1);
        }

        // tell Server to run get command
        write(socketfd, commandString, strlen(commandString));
        
        // Reading the acknowledgement being sent from the server
        char ackString[256];
        if(handleAcknowledge(socketfd, ackString) < 0) {
            exit(0);
        }

        // opening the file with the given pathname, if file does not exist, exit 
        int fd = open(pathname, O_RDWR, 0777);
        if(fd < 0) {
            fprintf(stderr, "%s\n", strerror(errno));
            exit(0);
        }

        // loop to write everything from the file on the client to the server
        int numberwritten;
        int numberread;

        char readBuf[256];
        numberread = read(fd, readBuf, 255);
        while(numberread != 0) {
            write(dataConnFd, readBuf, numberread);
            numberread = read(fd, readBuf, 255);
        }
        exit(0);
    }

}

void show(int socketfd, char *servHostName, char *pathname) {
     char commandString[256] =  "G"; // Stores the command that will be sent to the server which will be of the format "C<pathname>\n"
    char portString[256]; // Stores the port number or error message given in the acknowledgement from the server
    int dataConnFd;

    // adding pathname and newline character to the command
    strcat(commandString, pathname);
    strcat(commandString, "\n");

    // Writing to server to set up data connection
    write(socketfd, "D\n", 2);

    if(handleAcknowledge(socketfd, portString) < 0) {
        return;  
    }
    
    // printing out the more command from the ls of the server. 
    if(fork()) {
        int ret;
        wait(&ret);
        if(ret != 0) {
            exit(1);
        }
    } else {
        // note that the child will only exit with the exit code 1 if there was an error with regards to connecting to the server or creating the data connection. For all other errors or cases
        // it will simply exit with an exit code of 0. 
        // making a data connection
        dataConnFd = returnSocketFd(servHostName, portString);

        // if making the data connection fails then exit out of the child process 
        if(dataConnFd < 0) {
            exit(1);
        }

        // tell Server to run get command
        write(socketfd, commandString, strlen(commandString));
        
        // Reading the acknowledgement being sent from the server
        char ackString[256];
        if(handleAcknowledge(socketfd, ackString) < 0) {
            exit(0);
        }

        // this is the parent (which is reading from the child through the rdr file descriptor)
        wait(NULL);
        close(0); dup(dataConnFd); close(dataConnFd); // close stdin, then dup rdr which sets info of rdr to where stdin used to be, then close rdr. 
        execlp("more", "more", "-20", (char *)NULL);
        fprintf(stderr, "%s\n", strerror(errno));
        exit(1);
    }
}

void exitCommand(int socketfd) {
    char ackMessage[256];
    write(socketfd, "Q\n", 2);
    if(handleAcknowledge(socketfd, ackMessage) < 0) {
        exit(1);
    }
    exit(0);
}

// Function to connect to a server with the given server host name and port number parameters. Will return the socket file descriptor associated with that server. 
int returnSocketFd(char *servHostName, char *portNumber) {
    // Setting up the address of the server
    struct addrinfo hints, *actualdata;
    memset(&hints, 0, sizeof(hints));
    int err;	
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_family = AF_INET;
    err = getaddrinfo(servHostName, portNumber, &hints, &actualdata);
    
    if(err != 0) {
        fprintf(stderr, "Error: %s\n", gai_strerror(err));
        return -1;
    }
    
    int socketfd = socket(actualdata -> ai_family, actualdata -> ai_socktype, 0);   // returning the socket number
    if(socketfd < 0) {
        fprintf(stderr, "%s\n", strerror(errno));
        return -1;
    }

    // Connecting to the server
    if(connect(socketfd, actualdata -> ai_addr,actualdata -> ai_addrlen) < 0) {
        fprintf(stderr, "%s\n", strerror(errno));
        return -1;
    }

    return socketfd;
}

// Function to handle the acknowledgements sent from the server.
// must be given the socket file descriptor and also a character array with which the user wants to store
// the port number given from the acknowledgment (or the error message if there is one)
// if the acknowledgement begins with an E, it prints the error message and returns -1, otherwise it returns a 0.
int handleAcknowledge(int socketfd, char *ackString) {
    int numberread;
    char acknowledge[256];
    char buffer[256];
    char ackChar;

    // Reading the acknowledgement being sent fromt the server one character at a time
    int i = 0;
    while(1) {
        numberread = read(socketfd, buffer, 1);
        if(numberread == 0 || buffer[0] == '\n') break;
        acknowledge[i++] = buffer[0];
    }
    acknowledge[i] = '\0';

    // Parsing the acknowledgement into two parts, the first character and the rest of the acknowledgement
    sscanf(acknowledge, "%c %[^\n]", &ackChar, ackString);
        
    // Checking to see if acknowledgement was an E meaning an error occurred. 
    // Subsequently prints the error message of the error. 
    if(ackChar == 'E') {
        fprintf(stderr, "%s\n", ackString);
        return -1;
    }

    return 0;
}

// Function to print out the error when the user doesn't provide a required parameter to a command
void commandError() {
    printf("Command error: expecting a parameter.\n");
    printf("MFTP> ");
}
