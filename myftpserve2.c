#include "myftp.h"

#define PORT_NUMBER 11235
#define BACKLOG 4

int main(int argc, char **argv) {
	printf("Server is waiting for connections...\n");
	int listenfd = setConnection(BACKLOG, PORT_NUMBER);

	// initializing the variables before entering into the loop
	int connectfd;
	int length = sizeof(struct sockaddr_in);
	struct sockaddr_in clientAddr;

    while(1) {
		connectfd = accept(listenfd, (struct sockaddr *) &clientAddr, &length);
		if(connectfd < 0) {
			fprintf(stderr, "%s\n", strerror(errno));
			exit(1);
		}
		// Grabbing the hostName of the client
		char hostName[NI_MAXHOST];
		int hostEntry;
		hostEntry = getnameinfo((struct sockaddr*)&clientAddr, sizeof(clientAddr), hostName, sizeof(hostName), NULL, 0, NI_NUMERICSERV);
		if(hostEntry != 0) {
			fprintf(stderr, "%s\n", gai_strerror(hostEntry));
			exit(1);
		}
		// Grabbing the ip address of the client
		char *ip = inet_ntoa(clientAddr.sin_addr);

		pid_t childPid = fork();
		if(childPid) {
			printf("Child %d: Translation of client hostname -> %s\n", childPid, hostName);
			printf("Child %d: Client IP address -> %s\n", childPid, ip);
			printf("Child %d: Connection accepted from host %s\n", childPid, hostName);
			fflush(stdout);
			continue;
		} else {
			int numberread;
			char input[256] = {0};
			int dataConnFd;

			while((numberread = read(connectfd, input, 255)) != 0) {
				printf("%s\n", input);
				fflush(stdout);
				// Checking the given input and responding accordingly
				if(input[0] == 'D') {	// Creating a data connection
					printf("input D received, setting data connection\n");
					fflush(stdout);
					dataConnFd = setDataConnection(connectfd);
				/*************CD command*******************/
				} else if(input[0] == 'C') {	
					printf("input C received\n");
					fflush(stdout);
					char pathname[256]; parseInput(input, pathname);	// Grabbing the pathname given from the client
					// operate on cd normally now
					if(chdir(pathname) != 0) {
						// Dealing with errors here
						fprintf(stderr, "Change Directory: %s\n", strerror(errno));
						fflush(stderr);
						sendAcknowledgement(connectfd, 1, strerror(errno));
						memset(input, 0, 255);
						continue;
					}
					printf("Changed directory to %s\n", pathname);
					fflush(stdout);
					sendAcknowledgement(connectfd, 0, "");
				
				/*************ls command********************/
				} else if(input[0] == 'L') {	// Listing out the current directory
					printf("input L received\n");
					fflush(stdout);
					serveLs(connectfd, dataConnFd);
					// close(dataConnFd);
				/*************get command********************/
				} else if(input[0] == 'G') {	// Giving a file to the client
					printf("input G received\n");
					fflush(stdout);
					char pathname[256]; parseInput(input, pathname);	// Grabbing the pathname given from the client
					serveGet(connectfd, dataConnFd, pathname);
					// close(dataConnFd);
				/*************put command********************/
				} else if(input[0] == 'P' && input[numberread - 1] == '\n') {	// receiving a file from the client
					printf("input P received\n");
					fflush(stdout);
					char pathname[256]; parseInput(input, pathname);	// Grabbing the pathname given from the client
					servePut(connectfd, dataConnFd, pathname);
					// close(dataConnFd);
				/*************exit command********************/
				} else if(input[0] == 'Q' && input[1] == '\n') {	// Exiting out of the server
					printf("Input Q received\n");
					fflush(stdout);
					sendAcknowledgement(connectfd, 0, "");
					exit(0);
				}
				// removing garbage from input buffer
				memset(input, 0, 255);
			}
			exit(0);
		}
	}
    return 0;
}

void serveLs(int socketfd, int dataConnFd) {
	// sending positive acknowledgement before using exec for the ls command
	sendAcknowledgement(socketfd, 0, "");
	if(fork()) {
		wait(NULL);
	} else {
		close(1); dup(dataConnFd); close(dataConnFd);
		execlp("ls", "ls", "-l", (char *)NULL);
		fprintf(stderr, "%s\n", strerror(errno));
		exit(0);
	}
	close(dataConnFd);
}


void serveGet(int socketfd, int dataConnFd, char *pathname) {
	 // Checking if the given file is a directory
    struct stat area; struct stat* s = &area;

    // first checks if the path for s is accessible
    if(access(pathname, R_OK | F_OK) < 0) {
        fprintf(stderr, "%s\n", strerror(errno));
		fflush(stderr);
		sendAcknowledgement(socketfd, 1, strerror(errno));
        return;
    }

    // Checking to make sure that the pathname given isn't to a directory (if so, ignore the file and continue)
    if (lstat(pathname, s) == 0) {
        if (S_ISDIR(s->st_mode)) {
            fprintf(stderr, "%s\n", strerror(EISDIR));
			fflush(stderr);
			sendAcknowledgement(socketfd, 1, strerror(EISDIR));
            return;
        }
    } else {
        fprintf(stderr, "%s\n", strerror(errno));
		fflush(stderr);
		sendAcknowledgement(socketfd, 1, strerror(errno));
        return;
    }

	// opening the file with the given pathname, if file does not exist, exit 
	int fd = open(pathname, O_RDWR, 0777);
	if(fd < 0) {
		fprintf(stderr, "%s\n", strerror(errno));
		sendAcknowledgement(socketfd, 1, strerror(errno));
		return;
	}

	// Sending the positive acknowledgement after checking the 
	sendAcknowledgement(socketfd, 0, "");

	// loop to write everything from the file on the server to the client
	int numberwritten;
	int numberread;

	char readBuf[256];
	numberread = read(fd, readBuf, 255);
	while(numberread != 0) {
		write(dataConnFd, readBuf, numberread);
		numberread = read(fd, readBuf, 255);
	}
	close(dataConnFd);
}

void servePut(int socketfd, int dataConnFd, char *pathname) {
	    // opening a file of the same name of the file in the server
        // using O_CREAT and O_EXCL in order to make sure the file being created doesn't exist
        int fd = open(pathname, O_RDWR | O_CREAT | O_EXCL, 0777);
        if(fd < 0) {
            fprintf(stderr, "%s\n", strerror(errno));
			sendAcknowledgement(socketfd, 1, strerror(errno));
            return;
        }
		
		sendAcknowledgement(socketfd, 0, "");

        // loop to write everything from server to a new file created
        int numberread;
        char readBuf[256];
        numberread = read(dataConnFd, readBuf, 255);
        while(numberread != 0) {
            write(fd, readBuf, numberread);
            numberread = read(dataConnFd, readBuf, 255);
        }
		close(dataConnFd);
}


// Returns the file descriptor used for the listen function, or returns the portNumber if retPort is set to 1 
int setConnection(int backlog, int portNumber) {
	// Make a socket
	int listenfd;
	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &(int){1},
	sizeof(int));

	// Bind the socket to a port
	struct sockaddr_in servAddr;
	memset(&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(portNumber);
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind( listenfd, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
				fprintf(stderr, "%s\n", strerror(errno));
				fflush(stderr);
				exit(1);
	}

	// allow the socket to listen and establish the process as a server
	if(listen(listenfd, backlog) < 0) {
		fprintf(stderr, "%s\n", strerror(errno));
		fflush(stderr);
		exit(1);
	}

	return listenfd;
}

// function to set the data connection.
// returns the data connection fd (file descriptor), must be given the control connection socket fd 
int setDataConnection(int socketfd) {
	int portNumber; char portString[256];	// integer to store port number and also buffer to store the string version of the portNumber
	char acknowledge[256];	// acknowledgement buffer
	int listenfd;
	int dataConnFd;

	socklen_t sockaddrSize = sizeof(struct sockaddr_in);

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &(int){1},
	sizeof(int));

	// Bind the socket to a port
	struct sockaddr_in servAddr;
	memset(&servAddr, 0, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(0);
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind( listenfd, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
				fprintf(stderr, "%s\n", strerror(errno));
				fflush(stderr);
				sendAcknowledgement(socketfd, 1, strerror(errno));
				return -1;
	}

	// getting info of the socket
	getsockname(listenfd, (struct sockaddr *)&servAddr, &sockaddrSize);

	// allow the socket to listen and establish the process as a server
	if(listen(listenfd, 1) < 0) {
		fprintf(stderr, "%s\n", strerror(errno));
		fflush(stderr);
		sendAcknowledgement(socketfd, 1, strerror(errno));
		return -1;
	}

	// Grabbing port number and sending it through an acknowledgement
	portNumber = ntohs(servAddr.sin_port);
	sprintf(portString, "%d", portNumber);
	sendAcknowledgement(socketfd, 0, portString);

	// waiting to connect to the client
	int length = sizeof(struct sockaddr_in);
	struct sockaddr_in clientAddr;
	dataConnFd = accept(listenfd, (struct sockaddr *) &clientAddr, &length);
	if(dataConnFd < 0) {
		fprintf(stderr, "%s\n", strerror(errno));
		fflush(stderr);
		sendAcknowledgement(socketfd, 1, strerror(errno));
		return -1;
	}

	return dataConnFd;
}

// Sends the acknowledgement to the client through the socket file descriptor
// Must pass the socket file descriptor and an integer which determines whether the acknowledgement to be sent is an error or not
// isErr should be 1 to represent an error, and 0 to represent no errors, and then also the error message or portNumber must be passed
// (a null terminator should be passed if there is no need for error messages or port numbers) 
void sendAcknowledgement(int socketfd, int isErr, char *errorOrPort) {
	if(isErr) {
		char acknowledge[256] = "E";
		strcat(acknowledge, errorOrPort);
		strcat(acknowledge, "\n");
		printf("Sending Acknowledgement: %s\n", acknowledge);
		fflush(stdout);
		write(socketfd, acknowledge, strlen(acknowledge));
	} else {
		char acknowledge[256] = "A";
		if(strlen(errorOrPort) != 0) {
			strcat(acknowledge, errorOrPort);
		}
		strcat(acknowledge, "\n");
		printf("Sending Acknowledgement: %s\n", acknowledge);
		fflush(stdout);
		write(socketfd, acknowledge, strlen(acknowledge));
	}
}

// Parses the server commands and fills the given pathname parameter with the pathname given from the command. 
// This function should only be used if the client sent a command containing a pathname within it.
void parseInput(char *input, char *pathname) {
	printf("hello I'm in here1\n");
	// looping through every character of the input in order to add a null terminator at the end
	// must do this to aoid weird string issues
	int i = 0;
	while(1) {
		if(input[i] == '\n') {
			++i;
			break;
		}
		++i;
	}
	input[i] = '\0';

	char firstChar;
	char parsedPathname[256];

	sscanf(input, "%c %[^\n]", &firstChar, parsedPathname);
	strcpy(pathname, parsedPathname);
	printf("%s\n", pathname);
	printf("hello I'm in here2\n");
	fflush(stdout);
}